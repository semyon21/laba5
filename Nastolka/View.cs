﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using library;
using System.CodeDom;

namespace Nastolka
{
    public class View : INotifyPropertyChanged
    {
        Class1 square = new Class1();



        public double GetSq(double OutR, double InR, double a)
        {
            if (OutR >= InR)
            {
                return new Class1().GetSq(OutR, InR, a);
            }
            else
            {
                throw new ArgumentException("Внешний радус должен быть больше внутреннего");

            }
        }



        private double _OutR;
        public double OutR
        {
            get { return _OutR; }
            set
            {
                
                _OutR = value;
                OnPropertyChanged("S");
            }
        }

     
        private double _InR;
        public double InR
        {
            get { return _InR; }
            set
            {
                _InR = value;
                OnPropertyChanged("S");
            }
        }
        private double _a;

        

        public double a
        {
            get { return _a; }
            set
            {
                _a = value;
                OnPropertyChanged("S");
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        }
        public string S { get => View.GetSq(OutR, InR, a).ToString(); }
    }
}



        