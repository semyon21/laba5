﻿using System;
using System.ComponentModel;

namespace library
{

    public class Class1 : INotifyPropertyChanged

    {
        public Class1() { }




        public static double GetSq(double OutR, double InR, double a) => (Math.PI * a / 360) * (Math.Pow(OutR, 2) - Math.Pow(InR, 2));

        private double _OutR;
        public double OutR
        {
            get { return _OutR; }
            set
            {
                _OutR = value;
                OnPropertyChanged("S");
            }
        }
        private double _InR;
        public double InR
        {
            get { return _InR; }
            set
            {
                _OutR = value;
                OnPropertyChanged("S");
            }
        }

        private double _a;

       

        public double a
        {
            get { return _a; }
            set
            {
                _OutR = value;
                OnPropertyChanged("S");
            }
        }

        private void OnPropertyChanged(string v)
        {
            throw new NotImplementedException();
        }

        public event PropertyChangedEventHandler PropertyChanged;

    }
}



