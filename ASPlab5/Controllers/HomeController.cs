﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ASPlab5.Models;
using System.Security.Cryptography.X509Certificates;
using library;

namespace ASPlab5.Controllers
{
    public class HomeController : Controller
    {

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

            [HttpPost]
            public IActionResult Index(string OutR, string InR, string a)
        {
            double Square = Class1.GetSq(Convert.ToDouble(OutR), Convert.ToDouble(InR), Convert.ToDouble(a));
            ViewBag.result = Square;
            return View();
        }

    }
}
